var app = require('express')(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    ent = require('ent'), // Permet de bloquer les caractères HTML (sécurité équivalente à htmlentities en PHP)
    fs = require('fs'),
    db = require('./mysql');

var express = require('express'),
    exphbs = require('express-handlebars'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    session = require('express-session'),
    passport = require('passport'),
    LocalStrategy = require('passport-local'),
    TwitterStrategy = require('passport-twitter'),
    GoogleStrategy = require('passport-google'),
    FacebookStrategy = require('passport-facebook');


var connect = require('connect');
var bodyParser = require('body-parser');

// parse urlencoded request bodies into req.body
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

//SESSION - IDENTIFICATION
app.use(logger('combined'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(session({secret: 'supernova', saveUninitialized: true, resave: true}));
app.use(passport.initialize());
app.use(passport.session());
// Session-persisted message middleware
app.use(function (req, res, next) {
    var err = req.session.error,
        msg = req.session.notice,
        success = req.session.success;

    delete req.session.error;
    delete req.session.success;
    delete req.session.notice;

    if (err) res.locals.error = err;
    if (msg) res.locals.notice = msg;
    if (success) res.locals.success = success;

    next();
});
// Configure express to use handlebars templates
var hbs = exphbs.create({
    defaultLayout: 'main', //we will be creating this layout shortly
});
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');


/*app.get('/login', function (req, res) {
 res.end("NOT HOLA");
 db.query('INSERT INTO Message SET ?', postMessage, function(err, result) {
 if (err) throw err;
 });
 });*/



//Connexion
app.post('/login', passport.authenticate('local-signin', {
        successRedirect: '/tchat',
        failureRedirect: '/signin'
    })
);

//Deconnexion
app.get('/logout', function (req, res) {
    var name = req.user.username;
    console.log("LOGGIN OUT " + req.user.username)
    req.logout();
    res.redirect('/');
    req.session.notice = "Deconnexion reussi ! ";
});

//sends the request through our local signup strategy, and if successful takes user to homepage, otherwise returns then to signin page
app.post('/local-reg', passport.authenticate('local-signup', {
        successRedirect: '/',
        failureRedirect: '/signin'
    })
);

//Routes
//Page du tchat

app.post('/resultTchatForm', function (req, res) {
    var message = req.body.msgToSave;
    var idUser = req.body.idUser;
    var idCorrespondant = req.body.idCorrespondant;
    var now = new Date();
    var postMessage = {Texte: message, Heure: now, idEmetteur: idUser, idRecepteur: idCorrespondant};
    db.query('INSERT INTO message SET ?', postMessage, function (err, result) {
        if (err) throw err;
    });
 });
//Temps Reel
io.sockets.on('connection', function (socket, pseudo) {
    // Dès qu'on nous donne un pseudo, on le stocke en variable de session et on informe les autres personnes
    socket.on('nouvelle_connexion', function (pseudo) {
        pseudo = ent.encode(pseudo);
        socket.pseudo = pseudo;
        socket.broadcast.emit('nouvelle_connexion', pseudo);
    });

    // Dès qu'on reçoit un message, on récupère le pseudo de son auteur et on le transmet aux autres personnes
    socket.on('message', function (data) {
        message = ent.encode(data.message);
        socket.broadcast.emit('message', {idEmetteur: data.idEmetteur, message: message});
    });
});

server.listen(3000);