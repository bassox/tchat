<html>
  <head>
    <title>Inscription</title>
  </head>
  <body>

<?php

include 'header.php';
include 'function.php';

if (empty($_SESSION))  {

  if (empty($_POST['pseudo'])) // Si on la variable est vide, on peut considérer qu'on est sur la page de formulaire
  {
      echo '<h1>Inscription 1/2</h1>';
      echo '<form method="post" action="register.php" enctype="multipart/form-data" >
          <fieldset><legend>Identifiants</legend>
              <label for="pseudo">Pseudo :</label>  <input name="pseudo" type="text" id="pseudo" /><br />
              <label for="password">Mot de Passe :</label><input type="password" name="password" id="password" /><br />
          </fieldset>
          <fieldset><legend>Personnel</legend>
              <label for="nom">Nom :</label><input name="nom" type="text" id="nom" /><br />
              <label for="prenom">Prenom :</label><input type="text" name="prenom" id="prenom" /><br />
              <label for="promo">Promo : RIL (entre 15 et 17)</label><input type="number" name="promo" id="promo" /><br />
          </fieldset>
          <fieldset><legend>Autres</legend>
              <label for="email">Votre adresse Mail :</label><input type="text" name="email" id="email" /><br />
              <label for="avatar">Choisissez votre avatar :</label><input type="file" name="avatar" id="avatar" /><br />
          </fieldset>
          <p><input type="submit" value="S\'inscrire" /></p></form>
      </body>
      </html>';
  } //Fin de la partie formulaire
  else
  {
      $email_erreur1 = NULL;
      $email_erreur2 = NULL;
      $avatar_erreur = NULL;
      $avatar_erreur1 = NULL;
      $avatar_erreur2 = NULL;
      $avatar_erreur3 = NULL;

      $i = 0;
      $pseudo=$_POST['pseudo'];
      $nom = $_POST['nom'];
      $email = $_POST['email'];
      $prenom = $_POST['prenom'];
      $pass = $_POST['password'];
      $promo = $_POST['promo'];

      //Vérification du pseudo
      $query=$connexion->prepare('SELECT COUNT(*) AS nbr FROM utilisateur WHERE pseudo =:pseudo');
      $query->bindValue(':pseudo',$pseudo, PDO::PARAM_STR);
      $query->execute();
      $pseudo_free=($query->fetchColumn()==0)?1:0;
      $query->CloseCursor();
      if(!$pseudo_free)
      {
          $pseudo_erreur1 = "Votre pseudo est déjà utilisé par un membre";
          $i++;
      }

      $query=$connexion->prepare('SELECT COUNT(*) AS nbr FROM utilisateur WHERE coordonnees =:mail');
      $query->bindValue(':mail',$email, PDO::PARAM_STR);
      $query->execute();
      $mail_free=($query->fetchColumn()==0)?1:0;
      $query->CloseCursor();

      if(!$mail_free)
      {
          $email_erreur1 = "Votre adresse email est déjà utilisée par un membre";
          $i++;
      }

      //On vérifie la forme maintenant
      if (!preg_match("#^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-z]{2,4}$#", $email) || empty($email))
      {
          $email_erreur2 = "Votre adresse E-Mail n'a pas un format valide";
          $i++;
      }

	    if (!empty($_FILES['avatar']['size']))
	    {
	        //On définit les variables :
	        $maxsize = 80000; //Poid de l'image
	        $maxwidth = 800; //Largeur de l'image
	        $maxheight = 800; //Longueur de l'image
	        $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png', 'bmp' ); //Liste des extensions valides

	        if ($_FILES['avatar']['error'] > 0)
	        {
	                $avatar_erreur = "Erreur lors du transfert de l'avatar : ";
	        }

	        if ($_FILES['avatar']['size'] > $maxsize)
	        {
	                $i++;
	                $avatar_erreur1 = "Le fichier est trop gros : (<strong>".$_FILES['avatar']['size']." Octets</strong>    contre <strong>".$maxsize." Octets</strong>)";
	        }

	        $image_sizes = getimagesize($_FILES['avatar']['tmp_name']);
	        if ($image_sizes[0] > $maxwidth OR $image_sizes[1] > $maxheight)
	        {
	                $i++;
	                $avatar_erreur2 = "Image trop large ou trop longue :
	                (<strong>".$image_sizes[0]."x".$image_sizes[1]."</strong> contre <strong>".$maxwidth."x".$maxheight."</strong>)";
	        }

	        $extension_upload = strtolower(substr(  strrchr($_FILES['avatar']['name'], '.')  ,1));
	        if (!in_array($extension_upload,$extensions_valides) )
	        {
	                $i++;
	                $avatar_erreur3 = "Extension de l'avatar incorrecte";
	        }
	    }

      if ($i==0)
      {
          echo'<h1>Inscription terminée</h1>';
          echo'<p>Bienvenue '.stripslashes(htmlspecialchars($_POST['pseudo'])).' vous êtes maintenant inscrit sur le forum</p>';
          echo '<p>Cliquez <a href="./index.php">ici</a> pour revenir à la page d accueil</p>';


          $nomavatar=(move_avatar($_FILES['avatar']));

              $query=$connexion->prepare('INSERT INTO utilisateur (pseudo, motDePasse, coordonnees,
              nom, prenom, idPromotion, photo)
              VALUES (:pseudo, :pass, :email, :nom, :prenom, :idPromotion, :avatar)');
          $query->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
          $query->bindValue(':pass', $pass, PDO::PARAM_INT);
          $query->bindValue(':email', $email, PDO::PARAM_STR);
          $query->bindValue(':nom', $nom, PDO::PARAM_STR);
          $query->bindValue(':prenom', $prenom, PDO::PARAM_STR);
          $query->bindValue(':idPromotion', $promo, PDO::PARAM_INT);
          $query->bindValue(':avatar', $nomavatar, PDO::PARAM_STR);
          $query->execute();

      //Et on définit les variables de sessions
          $_SESSION['pseudo'] = $pseudo;
          $_SESSION['id'] = $connexion->lastInsertId(); ;
          $query->CloseCursor()
;      }
      else
      {
          echo'<h1>Inscription interrompue</h1>';
          echo'<p>Une ou plusieurs erreurs se sont produites pendant l incription</p>';
          echo'<p>'.$i.' erreur(s)</p>';
          echo'<p>'.$email_erreur1.'</p>';
          echo'<p>'.$email_erreur2.'</p>';
	      echo'<p>'.$avatar_erreur.'</p>';
	      echo'<p>'.$avatar_erreur1.'</p>';
	      echo'<p>'.$avatar_erreur2.'</p>';
	      echo'<p>'.$avatar_erreur3.'</p>';
          echo'<p>Cliquez <a href="./register.php">ici</a> pour recommencer</p>';
      }

  }
}else {
  header("Location: messagerie.php");
}
?>

</body>
</html>
