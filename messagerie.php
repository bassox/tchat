<?php include 'header.php'; ?>


<?php if (empty($_SESSION)) {

    header("Location: login.php");
}
?>


<div class="wrapper">
    <div class="container">
        <div class="left">
            <div class="top">
                <input type="text"/>
                <a href="javascript:;" class="search"></a>
            </div>
            <ul class="people">
                <?php

                //TODO Remplacer 1 par monIdUser
                //                while ($message = $listMessages->fetch()) {
                //
                //                    $monUtilisateur = $message->idRecepteur;
                //                    $corresondant = $message->idEmetteur;
                //
                $i = 0;
                while ($user = $listUsers->fetch()) {
                    $listUsersArray[$i++] = $user;

                    $nom = $user->nom;
                    $id = $user->idUtilisateur;
                    ?>
                    <li class="person" data-chat="person<?php echo $id ?>">
                        <img src="http://s13.postimg.org/ih41k9tqr/img1.jpg" alt=""/>
                        <span class="name"> <?php echo $nom ?> </span>
                        <span class="time"> 2:09 PM </span>
                        <span class="preview"> Converser avec <?php echo $nom ?></span>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>

        <div class="right">

            <div class="top"><span>To: <span class="name"></span></span></div>

            <?php
            foreach ($listUsersArray as $aUser) {
                $id = $aUser->idUtilisateur;
                //               $nom = $user['Nom'];
                //TODO : Remplacer 1 par un id
                $listMsgCorrespondant = $connexion->query("SELECT * FROM message WHERE idRecepteur = $idUser AND idEmetteur = $id");
                $listMsgEmetteur = $connexion->query("SELECT * FROM message WHERE idRecepteur = $id AND idEmetteur = $idUser");

                ?>
                <div class="chat" id="person<?php echo $id ?>" data-chat="person<?php echo $id ?>">
                    <div class="conversation-start">
                        <span>Today, 5:38 PM</span>
                    </div>
                    <?php
                    $listAllMsg = array();

                    while ($aMsg = $listMsgCorrespondant->fetch()) {
                        $arrayMsg['text'] = $aMsg['texte'];
                        $arrayMsg['class'] = 'you';
                        $listAllMsg[$aMsg['heure']] = $arrayMsg;

                        $msg = $aMsg['texte'];

                    }
                    while ($aMsg = $listMsgEmetteur->fetch()) {
                        $arrayMsg['text'] = $aMsg['texte'];
                        $arrayMsg['class'] = 'me';
                        $listAllMsg[$aMsg['heure']] = $arrayMsg;

                    }
                    ksort($listAllMsg);
                    foreach ($listAllMsg as $date => $oneMsg) {
                        ?>

                        <div class="bubble <?php echo $oneMsg['class'] ?>">
                            <?php echo $oneMsg['text'] ?>
                        </div>
                        <?php
                    } ?>
                </div>
                <?php
            }
            ?>
            <div class="write">
                <form action="http://localhost:3000/resultTchatForm" method="post" name="formulaire_chat"
                      id="formulaire_chat"
                      name="texteEnvoye">
                    <a href="javascript:;" class="write-link attach"></a>
                    <input type="text" id="texteAEnvoyer"/>
                    <input type="hidden" name="msgToSave" id="msgToSave"/>
                    <input type="hidden" name="idUser" id="idUser" value="<?php echo $idUser ?>"/>
                    <input type="hidden" name="idCorrespondant" id="idCorrespondant" value=""/>
                    <a href="javascript:;" class="write-link smiley"></a>
                    <a href="javascript:;" class="write-link send"></a>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="http://localhost:3000/socket.io/socket.io.js"></script>
<script src="theme/js/messagerie.js"></script>
</body>
