<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Messagerie | Trombi</title>
    <link rel="stylesheet" href="theme/css/materialize.css">
    <link rel="stylesheet" href="theme/css/main.css">
</head>
<body>
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="theme/js/jquery.min.js"></script>
<script type="text/javascript" src="theme/js/hammer.min.js"></script>
<script type="text/javascript" src="theme/js/materialize.js"></script>
<script type="text/javascript" src="theme/js/jsAppMsg.js"></script>

<?php session_start(); ?>

<header>
    <div class="page">
        <hgroup>
            <h1>Messagerie</h1>
            <h2>Trombinoscope</h2>
        </hgroup>
    </div><!-- End of page -->
    <nav class="bg-color-red-800">
        <div class="page">
            <ul>
                <li><a href="messagerie.php">Messagerie</a></li>
                <li><a href="trombinoscope.php">Trombinoscope</a></li>
                <?php
                if (!empty($_SESSION)) {
                    echo "<li><a href='deconnexion.php'>Déconnexion</a></li>";
                } else {
                    echo "<li> <a href='register.php'> Inscription </a> </li>
                      <li> <a href='login.php'> Connexion </a> </li>";
                }
                ?>


            </ul>
        </div><!-- End of page -->
    </nav>
</header>
<?php include 'database.php'; ?>