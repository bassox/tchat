<html>
<head>
    <title>Connexion</title>
</head>
<body>

<?php
include 'header.php';

//on vérifie si l'utilisateur est déjà connecté
if (isset($id) && $id != 0) echo 'Vous êtes déjà connecté';

if (!isset($_POST['pseudo'])) {
    echo '<form method="post" action="login.php">
        <fieldset>
            <legend>Connexion</legend>
            <p>
                <label for="pseudo">Pseudo :</label><input name="pseudo" type="text" id="pseudo" /><br />
                <label for="password">Mot de Passe :</label><input type="password" name="password" id="password" />
            </p>
        </fieldset>
        <p><input type="submit" value="Connexion" /></p></form>
    <a href="./register.php">Pas encore inscrit ?</a>
    </body>
    </html>';
} else {
    $message = '';
    if (empty($_POST['pseudo']) || empty($_POST['password'])) //Oublie d'un champ
    {
        $message = '<p>une erreur s\'est produite pendant votre identification.
    Vous devez remplir tous les champs</p>
    <p>Cliquez <a href="./login.php">ici</a> pour revenir</p>';
    } else //On check le mot de passe
    {
        $query = $connexion->prepare('SELECT motDePasse, idUtilisateur, pseudo FROM utilisateur WHERE pseudo = :pseudo');
        $query->bindValue(':pseudo', $_POST['pseudo'], PDO::PARAM_STR);
        $query->execute();
        $data = $query->fetch();
        if ($data['motDePasse'] == ($_POST['password'])) // Acces OK !
        {
            $_SESSION['pseudo'] = $data['pseudo'];
            $_SESSION['id'] = $data['idUtilisateur'];
            header("Location: messagerie.php");
        } else // Acces pas OK !
        {
            $message = '<p>Une erreur s\'est produite
            pendant votre identification.<br /> Le mot de passe ou le pseudo
                entré n\'est pas correcte.</p><p>Cliquez <a href="./login.php">ici</a>
            pour revenir à la page précédente
            <br /><br />Cliquez <a href="./index.php">ici</a>
            pour revenir à la page d accueil</p>';
        }
        $query->CloseCursor();
    }
    echo $message . '</div></body></html>';

}
?>
