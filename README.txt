Qui a fait quoi ?
Juliette : Front, ajax, javascript, module de messagerie sauf enregistrement BDD / socket.emit / socket.on
Alexis  RENON : Upload file, base de donnée, trombinoscope
Bassam : nodeJS, PHP, module de messagerie : enregistrement BDD / socket.emit / socket.on

Description projet : 
Application qui permet une communication 1vs1 entre utilisateurs d'une même promotion ou une promotion différente.



Lancer le projet : 
npm install => Installer les dépéndances
node app.js => lancer le serveur de tchat nodeJS

Les images sont stockées en local sur la machine, dans le dossier "/images/avatars/" sous la forme d'un timestamp.extension (exemple : 145849653.jpg). 
Les utilisateurs déjà créés n'auront donc pas accès à leurs images sur votre machine.


USERS :

login:UtilisateurTest mdp: test
login:Admin mdp:Admin