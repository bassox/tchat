// Connexion à socket.io
var socket = io.connect('http://localhost:3000');
var pseudo = "Saloute";
socket.emit('nouvelle_connexion', pseudo);
// Quand un nouveau client se connecte, on affiche l'information
socket.on('nouvelle_connexion', function (data) {
    $('.wrapper').append(data.pseudo + '<span id="userConnected">vient de se connecter ! </span>');
    //return false; // Permet de bloquer l'envoi "classique" du formulaire
});
// Quand un nouveau client se connecte, on affiche l'information
socket.on('message', function (data) {
    $('#person' + data.idEmetteur + '.chat').append("<div class='bubble you'>" + data.message + "</div>");
});
$(document).ready(function () {
    $(document).on('click', '.write-link.send', function () {
        $('#formulaire_chat').trigger('submit');
    });
    // Lorsqu'on envoie le formulaire, on transmet le message et on l'affiche sur la page
    $('#formulaire_chat').submit(function (event) {
        var person = $('.chat.active-chat').data('chat');
        var idPerson = person.split('person');
        $('#idCorrespondant').val(idPerson[1]);
        var message = $('#message').val();
        var valText = $('#texteAEnvoyer').val();
        var idUser = $('#idUser').val();
        $('.chat.active-chat').append("<div class='bubble me'>" + valText + "</div>");
        $('#texteAEnvoyer').val('').focus(); // Vide la zone de Chat et remet le focus dessus
        $('#msgToSave').val(valText);
        // get the form data
        socket.emit('message', {message: valText, idEmetteur: idUser}); // Transmet le message aux autrel
        // Quand on reçoit un message, on l'insère dans la page
        socket.on('message', function (data) {
            insereMessage(data.pseudo, data.message);
        });
        // here we will handle errors and validation messages
        var formData = {
            'idUser': $('input[name=idUser]').val(),
            'idCorrespondant': $('input[name=idCorrespondant]').val(),
            'msgToSave': $('input[name=msgToSave]').val()
        };
        // process the form
        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: 'http://localhost:3000/resultTchatForm', // the url where we want to POST
            data: formData, // our data object
            dataType: 'json', // what type of data do we expect back from the server
            encode: true
        });
        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });
});
// Ajoute un message dans la page
function insereMessage(pseudo, message) {
    $('#zone_chat').prepend('<p><strong>' + pseudo + '</strong> ' + message + '</p>');
}